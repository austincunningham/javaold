import java.util.Scanner;
public class Months {

	public static void main(String[] args) {
	Scanner MonthScanner = new Scanner(System.in);
	int months, numberOfDays=0;
	
	boolean isLeapYear;
	
	System.out.print("Which month?  ");
	months = MonthScanner.nextInt();
	
	switch (months){
	case 1:
	case 3:
	case 5:
	case 7:
	case 8:
	case 10:
	case 12:
		numberOfDays = 31;
		break;
		
	case 4:
	case 6:
	case 9:
	case 11:
		numberOfDays = 30;
		break;
		
	case 2:
		System.out.println("Leap year (true/false)?  ");
		isLeapYear = MonthScanner.nextBoolean();
		numberOfDays = isLeapYear ? 29 : 28;
	}
	System.out.print(numberOfDays);
	System.out.println(" days");
	}

}
