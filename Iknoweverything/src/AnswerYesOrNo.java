import java.util.Scanner;
import java.util.Random;

public class AnswerYesOrNo 
{

	public static void main(String[] args) 
	{
	Scanner yesnoScanner = new Scanner(System.in);
	Random yesnoRandom = new Random();
	int randomNumber;
	
	System.out.print("Type your question my child: ");
	yesnoScanner.nextLine();
	
	randomNumber = yesnoRandom.nextInt(10) + 1;
	System.out.println(randomNumber);
	
	if (randomNumber > 5) 
	{
		System.out.println("Yes. Isn't it obvious?");
	}
	else
	{
		System.out.println("No, and don't ask again.");
	}

	}

}
