package csvscan;

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

public class csvscan {

	public static void main(String[] args) 
		throws FileNotFoundException {
		
		Scanner diskScanner = new Scanner(new File ("c:/java/userlist.txt"));
		PrintStream diskWriter = new PrintStream ("c:/java/usernames.txt");
		char symbol;
		
		while (diskScanner.hasNext()){
		symbol = diskScanner.findWithinHorizon(".",0).charAt(0);
		if (symbol == '"'){
			symbol = ' ';
		}
		while (symbol != '@') {
			diskWriter.print(symbol);
			symbol = diskScanner.findWithinHorizon(".",0).charAt(0);
			
		}
		
		diskScanner.nextLine();
		diskWriter.println();

	    }
	}
}
