import java.util.Scanner;
class EchoLine {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(myScanner.nextLine());
		Scanner numScanner = new Scanner(System.in);
		System.out.println(numScanner.nextInt());
	}
}
